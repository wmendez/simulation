let magnitude = 1;
let prof = 1;
let waves = document.getElementById('waves');

function mw(magnit) {
	waves.style.width = magnit + 'px';
	waves.style.height = magnit + 'px';

	magnitude = magnit;
};

function profundity(kilometers){
	waves.style.top = kilometers + 'px';

	prof = kilometers;
};

const btns = document.getElementsByClassName("magnitudes");

function colorMw(x){

	for(var a of btns){
		a.style.background = 'white';
	}

	x.style.background = 'red';
}

const btns2 = document.getElementsByClassName("profundities");

function colorKm(x){

	for(var b of btns2){
		b.style.background = 'white';
	}

	x.style.background = 'red';
}

//earthquake effects

let city = document.getElementById('city');
let land = document.getElementById('land');

function update(){

	if(magnitude == 600 && prof <= -50){
		city.style.animationDuration = '1s';
		land.style.animationDuration = '1s';

		city.classList.remove('damaged');
		city.classList.remove('affected');
		city.classList.add('destroyed');

		land.classList.add('land__destroyed');
	}else if(magnitude == 600 && prof == 0){
		city.style.animationDuration = '2s';
		land.style.animationDuration = '2s';

		city.classList.remove('destroyed');
		city.classList.remove('affected');
		city.classList.add('damaged');
		land.classList.remove('land__destroyed');

	}else if(magnitude == 600 && prof > 0){
		city.style.animationDuration = '3s';
		land.style.animationDuration = '3s';

		city.classList.remove('destroyed');
		city.classList.remove('damaged');
		city.classList.add('affected');
		land.classList.remove('land__destroyed');

	}else if(magnitude == 500 && prof <= -120){
		city.style.animationDuration = '1s';
		land.style.animationDuration = '1s';

		city.classList.remove('damaged');
		city.classList.remove('affected');
		city.classList.add('destroyed');
		land.classList.add('land__destroyed');

	}else if(magnitude == 500 && prof <= 0 && prof > -120){
		city.style.animationDuration = '2s';
		land.style.animationDuration = '2s';

		city.classList.remove('destroyed');
		city.classList.remove('affected');
		city.classList.add('damaged');
		land.classList.remove('land__destroyed');

	}
	else if(magnitude == 500 && prof > 0 ){
		city.style.animationDuration = '3s';
		land.style.animationDuration = '3s';

		city.classList.remove('destroyed');
		city.classList.remove('damaged');
		city.classList.add('affected');
		land.classList.remove('land__destroyed');

	}else if(magnitude == 400 && prof <= -50 ){
		city.style.animationDuration = '2s';
		land.style.animationDuration = '2s';

		city.classList.remove('destroyed');
		city.classList.remove('affected');
		city.classList.add('damaged');
		land.classList.remove('land__destroyed');

	}else if(magnitude == 400 && prof <= 50 && prof > -50 ){
		city.style.animationDuration = '3s';
		land.style.animationDuration = '3s';

		city.classList.remove('destroyed');
		city.classList.remove('damaged');
		city.classList.add('affected');
		land.classList.remove('land__destroyed');

	}else if(magnitude == 350 && prof <= -120 ){
		city.style.animationDuration = '3s';
		land.style.animationDuration = '3s';

		city.classList.remove('destroyed');
		city.classList.remove('affected');
		city.classList.add('damaged');
		land.classList.remove('land__destroyed');

	}else if(magnitude == 350 && prof <= 0 && prof > -120 ){
		city.style.animationDuration = '4s';
		land.style.animationDuration = '4s';

		city.classList.remove('destroyed');
		city.classList.remove('damaged');
		city.classList.add('affected');
		land.classList.remove('land__destroyed');

	}else if(magnitude == 350 && prof >= 120 ){
		city.style.animationDuration = '10s';
		land.style.animationDuration = '10s';

	}else if(magnitude == 300 && prof <= -160 ){
		city.style.animationDuration = '4s';
		land.style.animationDuration = '4s';

		city.classList.remove('destroyed');
		city.classList.remove('damaged');

		city.classList.add('affected');
		land.classList.remove('land__destroyed');

	}else if(magnitude == 250 && prof <= -120 ){
		city.style.animationDuration = '8s';
		land.style.animationDuration = '8s';
		city.classList.remove('damaged');
		city.classList.remove('affected');
		city.classList.remove('destroyed');
	}else if(magnitude == 200 && prof <= -160 ){
		city.style.animationDuration = '10s';
		land.style.animationDuration = '10s';
		city.classList.remove('damaged');
		city.classList.remove('affected');
		city.classList.remove('destroyed');
		land.classList.remove('land__destroyed');
	}else if(magnitude <= 150 ){
		city.style.animationDuration = '0s';
		land.style.animationDuration = '0s';
		city.classList.remove('damaged');
		city.classList.remove('affected');
		city.classList.remove('destroyed');
		land.classList.remove('land__destroyed');
	}
}

//modal
let infoBtn = document.getElementById('infoBtn');


infoBtn.addEventListener("click", function(){
	let modal = document.getElementById("modal");
	modal.style.display = "flex";
	
	let modalContainer = document.getElementById("modalContainer");

	setTimeout((function(){
		modalContainer.style.opacity = "1";
	}), 300);
})



/**************/

let cross = document.getElementById("exit");

cross.addEventListener("click", function(){
	modalContainer.style.opacity = "0";
	
	setTimeout((function(){
		modal.style.display = "none";
	}), 1000);
})